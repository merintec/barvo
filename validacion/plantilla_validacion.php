<!DOCTYPE html>

<html lang="es">


    <head>

        <meta charset="utf-8" />

        <title>Prueba de validaciones</title>

    </head>
    
<script src="validacion/js/jquery-1.8.2.min.js" type="text/javascript"></script>
<script src="validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
<script src="validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="validacion/css/validationEngine.jquery.css" type="text/css"/>
<link rel="stylesheet" href="validacion/css/template.css" type="text/css"/>
 <link href="css/estilos.css" rel="stylesheet" type="text/css">


<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> -->

<!-- validacion en vivo -->
<script >

	jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
		});

</script>
	
<!-- CALENDARIO-->
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			$( ".datepicker" ).datepicker();
			jQuery("form1").validationEngine();
		});
            
	
	</script>
	<!-- hasta aca calendario -->

    <body>

       <header><h1>PRUEBAS DE VALIDACIONES CON JQUERY-VALIDATE</h1></header>

        <nav>

            <ul>

                <li><a href="#">inicio</a></li>

                <li><a href="#">formularios</a></li>

                <li><a href="#">salir</a></li>

            </ul>

        </nav>
        <center>
          <div >
            <!--        <form id="form1" onsubmit="return jQuery(this).validationEngine('validate');" class="formular" method="post"> -->
          <form id="form1" onsubmit="return jQuery(this).validationEngine('validate');" class="formular" method="post">
          <label> Nombres y Apellidos: </label>
				<input value="" class="validate[required,custom[onlyLetterSp],minSize[3],maxSize[30]] text-input, cajas_entrada" type="text" name="nombre" id="nombre" placeholder="Nombres y Apellidos" />
          </br>
          </br>          
          <label> Edad: </label>
				<input value="" class="validate[required,custom[integer],min[5],max[20]] text-input, cajas_entrada" type="text" name="edad" id="edad" data-prompt-position="topLeft" />
          </br>
          </br>
           <label> monto: </label>
				<input value="" class="validate[required,custom[number]] text-input" type="text" name="monto" id="monto" />
          </br>
          </br>
          
          <label> Correo: </label> <input value="" class="validate[required,custom[email]]" type="text" name="email" id="email" 
                       data-errormessage-value-missing="Debe ingresar el correo electronico" 
    						  data-errormessage-custom-error="Debe contener el siguiente formato: nombre@dominio.com" />
  							 
          </br>
          </br>
			 <label> Fecha: </label> <input value="" class="validate[required,custom[date]] text-input datepicker"  type="text" name="date" id="date" />
			 </br>
			 </br>
			 <label> Nombre usuario: </label> <input value="" class="validate[required,custom[onlyLetterNumber]]" type="text" name="special" id="special" />
          </br>
          </br>
          <label>		Estado Civil: </label>
				<select name="ecivil" id="ecivil"  class="validate[required]">
					<option value="">Seleccione...</option>
					<option value="option1">Soltero</option>
					<option value="option2">Casado</option>
					<option value="option3">Viudo</option>
					<option value="option4">Divorciado</option>
				</select>
			</br>
			</br>
  <label>Telefono:</label>
  				<input value="" class="validate[custom[phone]] text-input" type="text" name="telefono" id="telefono" />
			</br>
			</br>
	<label>Sexo</label> : 
					
				<input class="validate[required] radio" type="radio" name="sexo" id="masculino" value="m"/><span>Masculino: </span>
				<input class="validate[required] radio" type="radio" name="sexo" id="femenino" value="f"/><span>Femenino: </span>			
			</br>
			</br>
			
	<label>deportes favoritos</label>
	
		<input class="validate[minCheckbox[1]] checkbox" type="checkbox" name="deporte" value="futbol" id="deporte-1"/>Futbol 
			<input class="validate[minCheckbox[1]] checkbox"  type="checkbox" name="deporte" value="balonceto" id="deporte-2"/>Baloncesto 
			<input class="validate[minCheckbox[1]] checkbox"  type="checkbox" name="deporte" value="beisbol" id="deporte-3"/>Beisbol 
			 </br>
			 </br>
	<label>Foto:</label>
				<input class="validate[required] text-input" type="file" name="foto" id="foto" data-prompt-position="inline"/>
			 </br>
			 </br>
          <input type="submit" value="Guardar" />
          </form>
          
          
          </div>
      
        
        </center>
      

        <footer>

            <small>

                Copyright &copy; AC Merintec RL 2013<br/>

             

            </small>        

        </footer>

    </body>

</html>