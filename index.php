<?php
include("comunes/variables.php");
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?php echo $var_descripcion; ?>" />
    <meta name="author" content="<?php echo $var_author; ?>" />
    <meta property="og:description" content="<?php echo $var_descripcion; ?>" />    
    <meta property="og:title" content="Barvo. Fábrica de alimentos especializada en salsas para pastas y pastas frescas congeladas,  Mérida Venezuela" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="http://barvo.com.ve/prueba/imagenes/site/web-barvo.png" />
    <meta property="og:image" content="http://barvo.com.ve/prueba/imagenes/site/logo_tn.png" />
    <meta property="og:url" content="http://www.barvo.com.ve/" />
    <meta property="og:site_name" content="Barvo" />
    <meta property="og:locale" content="es_LA" />
    <meta name="keywords" content="<?php echo $page_keywords; ?>" />
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.js"> </script>
    <script src="validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="validacion/css/template.css" type="text/css"/>
    <link rel="stylesheet" href="estilo/swiper.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="estilo/estilo.css">
    <!-- JSON-LD para el marcado de datos estructurados de Google. -->
    <script type="application/ld+json">
    {
      "@context" : "http://schema.org",
      "@type" : "Organization",
      "name" : "Barvo",
      "description": "<?php echo $var_descripcion; ?>",
      "image" : "http://barvo.com.ve/prueba/imagenes/site/logo-tn.png",
      "telephone" : "+584247191019",
      "email" : "info@barvo.com.ve",
      "address" : {
        "@type" : "PostalAddress",
        "addressLocality" : "Mérida",
        "addressRegion" : "Mérida",
        "addressCountry" : "Venezuela"
      },
      "contactPoint" : [
        { "@type" : "ContactPoint",
          "telephone" : "+5842471910198",
          "contactType" : "customer service",
          "areaServed" : ["VE"],
          "availableLanguage" : ["Spanish"]
        } ],
      "alumni": [
        {
          "@type": "Person",
          "name": "LUIS GABRIEL VALERO"
        }
      ],
      "url" : "http://www.barvo.com.ve/"
    }
    </script>
    <script type="text/javascript">
        function ir_aslide(swiper_name, indice){
            swiper_name.slideTo(indice, 1000, false);
        }

        function abrir_notibarvo(id_noti)
        {
            var url="frontend/contenido_noticias.php"; 
            var parametros = {
                "id_noti": id_noti
            }
           
            $.ajax
            ({
                type: "POST",
                url: url,
                data: parametros,
                success: function(data)
                {
                  
                  $("#contenido_modal").html(data);
                }
            });
            return false;

        }

        function abrir_receta(id_rece)
        {
            var url="frontend/contenido_recetas.php"; 
            var parametros = {
                "id_rece": id_rece
            }
           
            $.ajax
            ({
                type: "POST",
                url: url,
                data: parametros,
                success: function(data)
                {
                  
                  $("#contenido_modal").html(data);
                }
            });
            return false;

        }
    </script>
    <script type="text/javascript">
      // EVENTO CUANDO SE MUEVE EL SCROLL, EL MISMO APLICA TAMBIEN CUANDO SE RESIZA
      var change= false;
      $(window).scroll(function(){
        window_y = $(window).scrollTop(); // VALOR QUE SE HA MOVIDO DEL SCROLL
        scroll_critical = 110 // VALOR DE TU DIV
        if (window_y > scroll_critical) { // SI EL SCROLL HA SUPERADO EL ALTO DE TU DIV
          $("#menu-fijo").show();
        } else {
          $("#menu-fijo").hide();
        }
      });
    </script>
    <script type=text/javascript>
      function ir_a(a,correccion){
        if(a){
          var posicion = $("#"+a).offset().top;
          if(correccion){
            posicion = posicion - correccion;
          }
          $("html,body").animate({scrollTop:posicion},1000)}
        };
    </script>
    <link href="favicon.ico" rel="shortcut icon">
    <title><?php echo $title; ?></title>
    <link rel="icon" type="image/png" href="favicon.ico">
  </head>


  <body>
<nav class="inicial2 pull-right" id="menu-fijo">
  <img src="imagenes/site/rama-menu.png" class="hidden-xs" id="rama-menu">
  <div class="redes hidden-xs">
    <div class="redes_boton redes_sombra"></div>
    <div class="redes_boton redes_sombra"></div>
    <div class="redes_boton redes_sombra"></div>
    <div style="float: right; margin: 2px;"></div>
    <div class="redes_boton redes_sombra"></div>
  </div>
  <div class="redes hidden-xs">
    <a target="new" href="#"><div class="redes_boton"><center><img src="imagenes/site/icon-twitter.png" width="60%" style="margin-top:10px;"></center></div></a>
    <a target="new" href="https://www.facebook.com/Barvo-349570851740530/?ref=br_rs"><div class="redes_boton"><center><img src="imagenes/site/icon-face.png" width="30%" style="margin-top:7px;"></center></div></a>
    <a target="new" href="https://instagram.com/barvoca/"><div class="redes_boton"><center><img src="imagenes/site/icon-instagram.png" width="60%" style="margin-top:7px;"></center></div></a>
    <div style="float: right; margin: 2px;"></div>
    <a onclick="ir_a('tope');return false;" href="#"><div class="redes_boton"><center><img src="imagenes/site/icon-home.png" width="60%" style="margin-top:7px;"></center></div></a>
  </div>
  <div class="container-fluid">
    <div class="col-xs-6 col-md-2">
      <img class="logo"  src="imagenes/site/logo.png">
    </div>
    <div class="col-xs-6 col-md-9 text-center">    
      <?php include("frontend/menu_frontend.php"); ?>
    </div>
  </div>
</nav>
<script type="text/javascript">
  $("#menu-fijo").hide();
</script>

<nav class="inicial pull-right hidden-xs" data-spy="affix" data-offset-top="110">
  <div class="posicion_redes">
    <a target="new" href="https://instagram.com/barvoca/"><div class="redes_sociales"><center><img src="imagenes/site/icon-instagram.png" width="60%" style="margin-top:7px;"></center></div></a>
    <a target="new" href="https://www.facebook.com/Barvo-349570851740530/?ref=br_rs"><div class="redes_sociales"><center><img src="imagenes/site/icon-face.png" width="30%" style="margin-top:7px;"></center></div></a>
    <a target="new" href="#"><div class="redes_sociales"><center><img src="imagenes/site/icon-twitter.png" width="60%" style="margin-top:10px;"></center></div></a>
  </div>
</nav>

<div class="tope" id="tope">
  <img id="tope_esquina" src="imagenes/site/tope-esquina.png">  
  <div class="tope_back"><img id="logo"  src="imagenes/site/logo.png"></div>
  <div class="menu_back"><div class="posicion_menu"><?php include("frontend/menu_frontend.php"); ?></div></div>
</div>


      
<div class="slide">
	<!-- <img  src="imagenes/site/slide0.jpg" width="100%"> -->
    <?php include("frontend/swiper.php"); ?> 
</div>

<div>

          <?php include("frontend/somos_barvo.php"); ?> 

</div>

<div>

          <?php include("frontend/productos.php"); ?>

</div>
<div>

          <?php include("frontend/noticias.php"); ?> 

</div>

<div>

          <?php include("frontend/entretenimiento.php"); ?> 

</div>

<div>

          <?php include("frontend/contacto.php"); ?> 

</div>

    <script src="bootstrap/js/bootstrap.min.js"> </script>  
     <!-- Swiper JS -->
    <script src="js/swiper.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
    var swiper = new Swiper('#swipertop', {
        pagination: '.swiper-pagination-top',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 0
    });
    </script>
    <script>
    var swiper = new Swiper('#swiper1', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    });
    </script>
     <script>
    var swiper2 = new Swiper('#swiper2', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 0,
        slidesPerView: 2

    });
    </script>
    <script>
    var swiper3 = new Swiper('#swiper3', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    });
    </script>
    <script>
    var swiper4 = new Swiper('#swiper4', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    });
    </script>

    <script type="text/javascript">
      ir_aslide(swiper, <?php echo $inicio; ?>);

      $("#videos").hide();
      
    </script>
  </body>
</html>
