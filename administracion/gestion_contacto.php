<?php
include("../comunes/variables.php");
include("verificar_admin.php");
include("../comunes/conexion.php");



?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../estilo/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
     <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
    <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <script type="text/javascript" src="../js/whatyouseeiswhatyouget/jquery-te-1.4.0.min.js" charset="utf-8"></script>
    <link type="text/css" rel="stylesheet" href="../js/whatyouseeiswhatyouget/jquery-te-1.4.0.css">
    <title><?php echo $nom_pagina; ?></title>
  </head>

  <script type="text/javascript">

   function buscar()
    {
      var url="busqueda_contacto.php"; 
      $.ajax
      ({
        type: "POST",
        url: url,
        data: $("#form1").serialize(),
        success: function(data)
        {
          $("#resultado_contacto").html(data);
        }
      });
      return false; 
    }

    function cambiar(id_cont,estado)
    {
         
          var parametros = {
            "estado":estado,
            "id_cont" : id_cont
          };
          var url="actualizar_contacto.php"; 
          $.ajax
          ({
            type: "POST",
              url: url,
              data: parametros,
              success: function(data)
              {
                $("#resultado_contacto").html(data);
                buscar();

              }
          });
           return false; 

    }


    


  </script>

   

<body>
    <?php
        include("menu_backend.php");
    ?>

<br>

<div class="container">
    <form method="POST" name="form1" id="form1">

      <div class="row" align="center">
         <div class="col-md-3">   </div> 
          <div class="col-md-2"> <label for="filtro" class="etq_form" >Filtrar:</label>  </div> 
          <div class="col-md-4"> <select name="filtro" id="filtro" class="form-control" onclick="buscar()";>
              <option value="" selected disabled style="display:none;">Seleccione...</option>
              <option  value="todos">Todos</option>
              <option  value="pendiente">Pendientes</option>
              <option  value="procesado">Procesados</option>
           </select>
          </div>
         <div class="col-md-3">  </div> 
          
      </div>
    </form>
    <br>
    <br>
    <div class="row">
      <div id='resultado_contacto' class="container">
        <?php 
          include("busqueda_contacto.php");
        ?>
      </div>
    </div>



</div>

  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>

