<?php
ini_set('post_max_size','10');
ini_set('upload_max_filesize','10');
ini_set('max_execution_time','99000');
ini_set('max_input_time','99000');
//echo ini_get('upload_max_filesize'). ", " . ini_get('post_max_size');
//echo ini_get('max_execution_time'). ", " . ini_get('max_input_time');
// defino la carpeta para subir
$uploaddir = '../imagenes/uploads/subproductos/';
// defino el nombre del archivo
$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
// Lo mueve a la carpeta elegida
if ($res = move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
  echo "<div class='alert alert-info' id='".$_POST[direc]."'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Imagen Cargada Exitosamente</strong></div>";
} else {
  echo "<div class='alert alert-danger' id='".$_POST[direc]."'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error al subir imagen</strong></div>";
}
?>