<?php
session_start();
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
include("verificar_admin.php");
include("../comunes/variables.php");
include("../comunes/conexion.php");

$tabla='usuarios';
$titulos_busqueda = 'Identificaci&oacute;n, Nombre - Apellido, Correo, Tipo Usuario, Status';
$datos_busqueda = 'numi_user,nom_ape_user,corre_user,tipo_user,stat_user';
$nregistros = 5;   




?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../estilo/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <title><?php echo $nom_pagina; ?></title>
  </head>
    <!-- validacion en vivo -->
<script>
  <!-- validacion en vivo -->
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
   });
</script>
<!-- CALENDARIO-->
<script>
    jQuery(document).ready(function(){
      // binds form submission and fields to the validation engine
      $( ".datepicker" ).datepicker();
      jQuery("form1").validationEngine();
    });
</script>
<!-- funcion de guardar formulario -->  
<script type="text/javascript">
$(function()
{
    $("#guardar").click(function()
    {
      if ($("#form1").validationEngine('validate')){
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form1").serialize(),
            success: function(data)
            {
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                $("#form1")[0].reset();
              }
              $("#resultado").html(mensaje);
              setTimeout(function() {
                $("#msg_act").fadeOut(1500);
              },3000);
              buscar();
            }
        });
        return false;
      }
    });
});
function buscar()
{
  var url="../comunes/busqueda.php"; 
  $.ajax
  ({
    type: "POST",
    url: url,
    data: $("#form1").serialize(),
    success: function(data)
    {
      $("#resultado_busqueda").html(data);
    }
  });
  return false; 
}

</script>

<body>
    <?php
        include("menu_backend.php");
    ?>
<div data-offset-top="100" class="container" data-spy="affix">
  <div id="resultado"></div>
</div>
<div class="jumbotron cajalogin">
      <div class="titulo_form">   Administración de Usuarios </div>
            <form method="POST" name="form1" id="form1" onsubmit="return jQuery(this).validationEngine('validate');">
                <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
                <input type="hidden" name="var_titulos_busqueda" id="var_titulos_busqueda" value='<?php echo $titulos_busqueda; ?>'>
                <input type="hidden" name="var_datos_busqueda" id="var_datos_busqueda" value='<?php echo $datos_busqueda; ?>'>
                <input type="hidden" name="var_nregistros" id="var_nregistros" value='<?php echo $nregistros; ?>'>
                <input type="hidden" name="var_pag_actual" id="var_pag_actual" value=''>   


                 <div class="form-group">
                  <label for="tipoi_user" class="etq_form" > Identificaci&oacute;n:</label> 
                   <div class="row">   
                       <div class="col-md-4">  
                       
                         <select name="tipoi_user" id="tipoi_user"   class="validate[required], form-control">';
                            <option value="" selected disabled style="display:none;"> Nac.</option>

                            <option   value="V" >V</option>
                            <option   value="E" >E</option>
                          </select>
                       
                      </div>
                        <div class="col-md-8">  
                          <div id="grupo_numi_user" class="">
                             <input type="text" name="numi_user" id="numi_user" class="validate[required, custom[integer], minSize[6], maxSize[20]] text-input, form-control" placeholder="N&uacute;mero Identificación">
                            <span id="boton_numi_user" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                            <span id="actualiza_numi_user" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                            </span>                       
                          </div>
                        </div>
                  </div>
                 </div> 

 
               
                  <div class="form-group">
                    <label for="nom_ape_user" class="etq_form" >Nombre y Apellido:</label>
                    <div id="grupo_nom_ape_user" class="input-group">                     
                     <input type="text" name="nom_ape_user" id="nom_ape_user" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[60]] text-input, form-control" placeholder="Nombre y Apellido del Usuario">
                     <span id="boton_nom_ape_user" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="buscar()";>
                        <span id="buscar_nom_ape_user" title="Buscar" class="glyphicon glyphicon-search"> </span>
                        <span id="actualiza_nom_ape_user" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                      </span>      
                    </div>
                  </div>

                   <div class="form-group">
                    <label for="tele_user" class="etq_form" >Tel&eacute;fono:</label>
                    <div id="grupo_tele_user" class="">
                          <input type="text" name="tele_user" id="tele_user" class="validate[required, custom[phone], minSize[6], maxSize[11]] text-input, form-control" placeholder="N&uacute;mero Telefonico">
                            <span id="boton_tele_user" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                            <span id="actualiza_tele_user" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                            </span>       
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="corre_user" class="etq_form" >Correo Electronico:</label>
                    <div id="grupo_corre_user" class="">
                       <input type="text" name="corre_user" id="corre_user" class="validate[required, custom[email], minSize[5], maxSize[100]] text-input, form-control" placeholder="Email">
                      <span id="boton_corre_user" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                      <span id="actualiza_corre_user" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                      </span>       
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="pass_user" class="etq_form" >Clave:</label>
                     
                       <input type="password" name="pass_user" id="pass_user" class="validate[required,equals[pass_user2]], minSize[5], maxSize[100]] text-input, form-control" placeholder="Clave de Acceso">
                        
                     
                    <div id="grupo_pass_user2" class="">
                        <input type="password" name="pass_user2" id="pass_user2" class="validate[required,equals[pass_user]] text-input, form-control" placeholder="Repetir Clave de Acceso">
                        <span id="boton_pass_user2" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                        <span id="actualiza_pass_user2" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                        </span>  
                    </div>
                  </div>

                  <div class="form-group">
                  <label for="tipo_user" class="etq_form" > Tipo de Usuario:</label> 
                   <div class="row">   
                       
                      <div id="grupo_tipo_user" class="input-group">

                         <select name="tipo_user" id="tipo_user"   class="validate[required], form-control">';
                         <option value="" selected disabled style="display:none;"> Tipo de usuario</option>
                            <option   value="1" >Administrador del Sistema</option>
                          </select>
                        <span id="boton_tipo_user" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="buscar()";>
                        <span id="buscar_tipo_user" title="Buscar" class="glyphicon glyphicon-search"> </span>                       
                         <span id="actualiza_tipo_user" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                        </span>  
                      </div>
                       
                      
                     </div>
                 </div> 

                 <div class="form-group">
                  <label for="stat_user" class="etq_form" > Status:</label> 
                   <div class="row">   
                       
                      <div id="grupo_stat_user" class="input-group">

                         <select name="stat_user" id="stat_user"   class="validate[required], form-control">';
                         <option value="" selected disabled style="display:none;"> Status de Usuario</option>
                            <option   value="activo" >Activo</option>
                            <option   value="inactivo" >Inactivo</option>
                          </select>
                        <span id="boton_stat_user" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="buscar()";>
                        <span id="buscar_stat_user" title="Buscar" class="glyphicon glyphicon-search"> </span>                       
                         <span id="actualiza_stat_user" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                        </span>  
                      </div>
                       
                      
                     </div>
                 </div> 

                


                <div align="center"><a href="<?php echo $_PHP_SELF; ?>"><button id="cancelar" type="button" class="btn btn_form oculto" >Cancelar</button></a>  <input type="submit" name="guardar"  id="guardar" value="Guardar" class="btn btn_form" > </div>
           
            </form>
          
           

</div>
<div class="row">
  <div id='resultado_busqueda' class="container">
    <?php 
      include("../comunes/busqueda.php");
    ?>
  </div>
</div>

 

  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>

