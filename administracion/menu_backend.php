<?php 
session_start();
$_SESSION['usuario_logueado'];
$_SESSION['tipo_usuario'];
include("verificar_admin.php");


?>
<div class="row" id="top_pagina">
  <div class="col-md-12">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

           <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuarios<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="usuarios.php">Registro de Usuarios</a></li>
            
                
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Swipers<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="swipers.php">Administrar Swiper</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Noticias<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="noticias.php">Administrar Noticias</a></li>
              </ul>
            </li>
          </ul>
           <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Productos <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="productos.php">Productos</a></li>
                <li><a href="sub-productos.php">Sub-Productos</a></li>
              </ul>
            </li>
            <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Recetas<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="tipos_recetas.php">Tipo de Recetas</a></li>
                <li><a href="recetas.php">Recetas</a></li>

              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="videos.php" role="button" aria-expanded="false">Videos</a>
              </li>
          </ul>
           <ul class="nav navbar-nav">
             <li class="dropdown">
              <a href="gestion_contacto.php" role="button">Contactos</a>
             </li>
           </ul>
           
            <ul class="nav navbar-right">
             <li class="dropdown">
              <a href="../comunes/cerrar_sesion.php"  >Cerrar Sesi&oacute;n (<?php echo $_SESSION['usuario_logueado'];  ?>) <span class="glyphicon glyphicon-user"> </span> </a>
               
            </li>
          </ul>

          
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
  </div>
</div>

