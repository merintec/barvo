<?php
include_once("../comunes/conexion.php");
include("../comunes/variables.php");
session_start();

$usuario=$_POST['usuario'];
$pass=md5($_POST['pass']);
$error=NULL;
if ($usuario!=NULL and $pass!=NULL) 
{ $sql = "SELECT * FROM usuarios WHERE corre_user = \"".mysql_real_escape_string($usuario)."\" AND pass_user = \"".mysql_real_escape_string($pass)."\"";
  $consulta_usuario=mysql_query($sql);
   $con_usuario=mysql_fetch_assoc($consulta_usuario);
   if ($con_usuario['corre_user']!=NULL and $con_usuario['stat_user']!='inactivo') 
   {
    
        $_SESSION['usuario_logueado'] = $con_usuario['nom_ape_user'];
        $_SESSION['tipo_usuario'] = $con_usuario['tipo_user'];
        $_SESSION['id_user'] = $con_usuario['id_user'];

        if ($con_usuario['tipo_user']==1){
          
          ?>
          <script type="text/javascript" >
              window.location=("principal.php");
            </script>
          <?php 
        }
        
  } 
  else 
  {
    if ($con_usuario['corre_user']==NULL)
    {
      $error = 'Email y/o contraseña incorrectos';
    }
    if ($con_usuario[stat_user]=='inactivo')
    {
      $error = 'Usuario Inactivo';
    }
  }



}
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../estilo/estilo.css">
   <!-- <script src="../validacion/js/jquery-1.8.2.min.js" type="text/javascript"></script>-->
   <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
    <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script >

  jQuery(document).ready(function(){
      // binds form submission and fields to the validation engine
      jQuery("#form1").validationEngine('attach', {bindMethod:"live"});
    });

</script>
<!-- CALENDARIO-->
<script>
    jQuery(document).ready(function(){
      // binds form submission and fields to the validation engine
      $( ".datepicker" ).datepicker();
      jQuery("form1").validationEngine();
    });
</script>


<body>
  <div class="jumbotron cajalogin">
    <div align="center"><img src="../imagenes/site/logo.png"  title="Logo" style="width: 8em;"></div>
      <div><h4 align="center"> Administración del Sitio</h3></div>
      <?php 
        if ($error!=NULL){
          echo '</br><div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>'.$error.'</strong></div>';
        }
      ?>
      <form method="POST" name="form1" id="form1" onsubmit="return jQuery(this).validationEngine('validate');">
        <div class="form-group">
          <label for="usuario" > Email</label>
           <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-user"> </span></span>
            <input type="email" name="usuario" id="usuario" class="validate[required, custom[email] , minSize[3],maxSize[100]] text-input, form-control" placeholder="Ingresar Email">
          </div>
        </div>
        <div class="form-group">
          <label for="pass"> Contraseña</label>
          <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
            <input type="password" name="pass" id="pass" class=" validate[required], form-control" placeholder="Contraseña">
          </div>
        </div>
        <div align="center"> <input type="submit" name="ingresar"  id="ingresar" value="Ingresar" class="btn btn-primary btn-lg btn-block" > </div>
      </form>
  </div>
  <script src="../bootstrap/js/bootstrap.min.js"> </script>
</body>
</html>