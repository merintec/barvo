<?php
include("../comunes/variables.php");
include("verificar_admin.php");
include("../comunes/conexion.php");
$tabla='tipo_recetas';
$titulos_busqueda = 'Nombre, Descripci&oacute;n, Orden';
$datos_busqueda = 'nomb_trece,desc_trece,orden';
$orden_busqueda = 'orden asc';
$edita_orden_num = 'orden';
$nregistros = 5;    
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../estilo/estilo.css">
    <script src="../bootstrap/js/jquery.js"> </script>
    <script src="../validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
    <script src="../validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="../validacion/css/validationEngine.jquery.css" type="text/css"/>
    <link rel="stylesheet" href="../validacion/css/template.css" type="text/css"/>
     <!-- para subir imagenes -->
    <script language="javascript" src="../js/AjaxUpload.2.0.min.js"></script>
    <!-- estos link se estan usando para el calendario, lo que si es que debemos descargarlos -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script> 
    <script type="text/javascript" src="../js/whatyouseeiswhatyouget/jquery-te-1.4.0.min.js" charset="utf-8"></script>
    <link type="text/css" rel="stylesheet" href="../js/whatyouseeiswhatyouget/jquery-te-1.4.0.css">
    <title><?php echo $nom_pagina; ?></title>
  </head>
  <!-- validacion en vivo -->
<script>
  <!-- validacion en vivo -->
  jQuery(document).ready(function(){
    // binds form submission and fields to the validation engine
    jQuery("#form10").validationEngine('attach', {bindMethod:"live"});
   });



</script>
<!-- funcion de guardar formulario -->  
<script type="text/javascript">
$(function()
{
    $("#guardar").click(function()
    {
      if ($("#form10").validationEngine('validate')){
        var url="../comunes/funcion_guardar.php"; 
        $.ajax
        ({
            type: "POST",
            url: url,
            data: $("#form10").serialize(),
            success: function(data)
            {
              var codigo, datatemp, mensaje;
              datatemp=data;
              datatemp=datatemp.split(":::");
              codigo=datatemp[0];
              mensaje=datatemp[1];
              if (codigo==001)
              {
                $("#form10")[0].reset();
               

              }
              $("#resultado").html(mensaje);
              buscar();
            }
        });
        return false;
      }
    });
});
function buscar()
{
  var url="../comunes/busqueda.php"; 
  $.ajax
  ({
    type: "POST",
    url: url,
    data: $("#form10").serialize(),
    success: function(data)
    {
      $("#resultado_busqueda").html(data);
    }
  });
  return false; 
}

</script>


<body>
    <?php
        include("menu_backend.php");
    ?>
<div data-offset-top="100" class="container" data-spy="affix">
  <div id="resultado"></div>
</div>
<div class="jumbotron cajaform">
      <div class="titulo_form">   Tipos de Recetas </div>
            <form method="POST" name="form10" id="form10" onsubmit="return jQuery(this).validationEngine('validate');">
                 <input type="hidden" name="var_tabla" id="var_tabla" value='<?php echo $tabla; ?>'>
                <input type="hidden" name="var_titulos_busqueda" id="var_titulos_busqueda" value='<?php echo $titulos_busqueda; ?>'>
                <input type="hidden" name="var_datos_busqueda" id="var_datos_busqueda" value='<?php echo $datos_busqueda; ?>'>
                <input type="hidden" name="var_orden_busqueda" id="var_orden_busqueda" value='<?php echo $orden_busqueda; ?>'>
                <input type="hidden" name="var_edita_orden_num" id="var_edita_orden_num" value='<?php echo $edita_orden_num; ?>'>

                <input type="hidden" name="var_nregistros" id="var_nregistros" value='<?php echo $nregistros; ?>'>
                <input type="hidden" name="var_pag_actual" id="var_pag_actual" value=''>  

                    
                <div class="form-group">
                    <label for="nomb_trece" class="etq_form" > Nombre:</label>
                    <div id="grupo_nomb_trece" class="input-group">
                        <input type="text" name="nomb_trece" id="nomb_trece" class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[100]] text-input, form-control" placeholder="Nombre del Tipo de Receta">
                        <span id="boton_nomb_trece" class="input-group-addon"  style="cursor:pointer; cursor: hand;" onclick="buscar()";>
                                    <span id="buscar_nomb_trece" title="Buscar" class="glyphicon glyphicon-search"> </span>
                                    <span id="actualiza_nomb_trece" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                        </span>       
                    </div>
                  </div>

                  <div class="form-group">
                   
                      <label for="desc_trece" class="etq_form" >Descripci&oacute;n:</label>
                      <div id="grupo_desc_trece" class="">
                          <input type="text" name="desc_trece" id="desc_trece" class="validate[required, custom[onlyLetterSp], minSize[4], maxSize[100]] text-input, form-control" placeholder="Descripci&oacute;n del tipo de receta">
                          <span id="boton_desc_trece" class="input-group-addon"  style="visibility:hidden; cursor:pointer; cursor: hand;" onclick="buscar()";>
                          <span id="actualiza_desc_trece" title="Guardar Cambios" class="glyphicon glyphicon-floppy-disk oculto"> </span>
                          </span>
                    </div>
                  </div>

                <div align="center"> <a href="<?php echo $_PHP_SELF; ?>"><button id="cancelar" type="button" class="btn btn_form oculto" >Cancelar</button></a>  <input type="submit" name="guardar"  id="guardar" value="Guardar" class="btn btn_form" > </div>
           
            </form>
          
           

</div>

<div class="row">
  <div id='resultado_busqueda' class="container">
    <?php 
      include("../comunes/busqueda.php");
    ?>
  </div>
</div>


  <script src="../bootstrap/js/bootstrap.min.js"> </script>
  </body>
</html>

