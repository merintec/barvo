<?php 
$nom_pagina="Barvo C.A.";
$correo_upalopa = 'info@barvo.com.ve';
$correo_compras = 'ventas@barvo.com.ve';
$var_author="http://merintec.com.ve";
$title="Barvo C.A. Fábrica de alimentos especializada en salsas para pastas y pastas frescas congeladas,  Mérida Venezuela";

$page_descripcion='Somos una fábrica de alimentos especializada en salsas para pastas y pastas frescas congeladas, dirigidas al segmento que busca variedad de opciones de alimentación rápidas y sencillas que les den el tiempo libre para disfrutar de las actividades que aman. Para ello contamos con un equipo de trabajo innovador y dedicado a ofrecer productos de excelente calidad.';
$page_keywords='Salsa Napolitana, Raviolis, Pasta, Pasticho, Barvo, Productos Barvo, Pizzas, Congelados, Pasta Congelada, Pasta Artesanal, Salsa Natural, Salsa Artesanal, Hecho En Mérida, Venezuela, fabrica, alimentos';
?>
