<!-- funcion de guardar formulario -->  
<script type="text/javascript">
function ordenar_orden(tabla,campo,valor,campo_id,valor_id,direccion)
{
	if (direccion=='sube'){		
		nuevo = (valor-1);
	}
	if (direccion=='baja'){		
		nuevo = (valor+1);
	}
		var parametros = {
			"var_tabla": tabla,
			"var_campo" : campo,
			"var_valor" : valor,
			"var_id" : campo,
			"var_id_val" : nuevo
		};
		var parametros2 = {
			"var_tabla": tabla,
			"var_campo" : campo,
			"var_valor" : nuevo,
			"var_id" : campo_id,
			"var_id_val" : valor_id
		};
      var url="../comunes/funcion_actualizarcampo.php"; 
      $.ajax
      ({
        type: "POST",
          url: url,
          data: parametros,
          success: function(data)
          {
          	console.log('primer dato');
          	$.ajax
		      ({
		        type: "POST",
		          url: url,
		          data: parametros2,
		          success: function(data2)
		          {
		          	console.log('segundo dato');
		          	location.reload();
		          }
		      });
          }
      });
      return false; 
}
function eliminar_registro(tabla,campo,valor)
{
	var parametros = {
		"var_campo" : campo,
		"var_valor" : valor,
		"var_tabla": tabla,
		"var_titulos_busqueda" : $('#var_titulos_busqueda').val(), 
		"var_datos_busqueda" : $('#var_datos_busqueda').val(),
		"var_edita_orden_num" : $('#var_edita_orden_num').val(), 
		"var_orden_busqueda" : $('#var_orden_busqueda').val(),
		"var_nregistros" : $('#var_nregistros').val(), 
		"var_pag_actual" : $('#var_pag_actual').val()
	};
	var url="../comunes/funcion_eliminar.php"; 
	$.ajax
	({
		type: "POST",
	    url: url,
	    data: parametros,
	    success: function(data)
	    {
   	   		var mensaje = "</br><div id='msg_act' class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='glyphicon glyphicon-ok-sign pull-left'></span>&nbsp;&nbsp;<strong>El Registro se eliminó exitosamente</strong></div>";
		    $("#resultado_busqueda").html(data);
		    $("#resultado").html(mensaje);
		    setTimeout(function() {
	        	$("#msg_act").fadeOut(1500);
	    	},4000);
	    }
	});
	return false; 
}

function llenar_form(tabla,campo,valor)
{
	$.post("../comunes/funcion_llenarform.php", {"var_tabla":tabla,"var_campo":campo,"var_valor":valor}, function(data){
   		var arr = data;
		for(var i=0;i<arr.length;i++){
	        var obj = arr[i];
	        for(var key in obj){
	        	var campo_destino;
	            var attrName = key;
	            var attrValue = obj[key];
	            if (!attrValue){
	            	attrValue = '';
	            }
	            //verificamos si tiene una regla de llenado procedemos a consultar y llenar
	            	campo_destino = $('#llenado_' + attrName).val();
	            	if (campo_destino){
	            		destinado = attrName;
						var parametros = {
							"valor" : attrValue,
							"dat" : campo_destino
						}
						var url="../comunes/funcion_llenar_combo.php"; 
						$.ajax
						({
							type: "POST",
						    url: url,
						    data: parametros,
						    success: function(data2)
						    {
					   	   		$("#" + destinado).html(data2);
						    }
						});
	            	} 
	            //para verificar si es una fecha voltearla al momento de mostrarla
	            var nueva;
	            nueva = attrValue.split("-");
	            if (attrValue.length == 10 && nueva[0].length == 4 && nueva[1].length == 2 && nueva[2].length == 2){
	            	attrValue = nueva[2]+'-'+nueva[1]+'-'+nueva[0];
	            }
	            var accion_posterior = "actualizar_campo('" + tabla + "', '" + attrName + "', $('#" + attrName + "').val(),'" + campo + "', " + valor + ")";
	            $("#" + attrName).val(attrValue);
	            $("#grupo_" + attrName).addClass("input-group");
	            $("#boton_" + attrName).css("visibility","visible");
	            $("#guardar").hide();
	            $("#cancelar").show();
	            $("#buscar_" + attrName).hide();
	            $("#actualiza_" + attrName).show();
	            $("#boton_" + attrName).attr("onclick", accion_posterior);	 
	           	if (attrName=='defecto'){
	           		if(attrValue=='s'){
	           			$("#"+attrName).prop("checked", "checked");
	           		}
	           		else{
	           			$("#"+attrName).prop("checked", "");
	           		}
	           	}
  	            if (attrName=='imag_prod')
	            {
	            	if (attrValue){
		            	$('#imagen_cargada').attr('src','../imagenes/uploads/productos/'+attrValue);
	            	}
	            	else{
	            		$('#imagen_cargada').attr('src','');	
	            	}

	            }   
  	            if (attrName=='icon_prod')
	            {
	            	if (attrValue){
		            	$('#imagen_cargada1').attr('src','../imagenes/uploads/productos/'+attrValue);
	            	}
	            	else{
	            		$('#imagen_cargada1').attr('src','');	
	            	}

	            }      
  	           
	        }
	        //$("#cont_prod").jqte();
	        //para volver a cargar el valor del txt
	        if ($(".jqte_editor"))
	        {
	         $(".jqte_editor").jqteVal($(".jqte-test").val());
	        }


   		}
   		var mensaje = "</br><div id='msg_act' class='alert alert-warning'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='glyphicon glyphicon-alert pull-left'></span>&nbsp;&nbsp;<strong>Actualice la información que requiera y presione el botón que acompaña el campo para guardar el cambio</strong></div>";
   		$("#resultado").html(mensaje);
   		
	},"json");
	$('html,body').animate({
        scrollTop: $("#top_pagina").offset().top
    }, 500);
    setTimeout(function() {
        $("#msg_act").fadeOut(1500);
    },10000);
}
function actualizar_campo(tabla,campo,valor,campo_id,valor_id)
{
	if ($("#form1").validationEngine('validate'))
	{

	        //para verificar si es una fecha voltearla al momento de guardarla
            var nueva;
            nueva = valor.split("-");
            if (valor.length == 10 && nueva[0].length == 2 && nueva[1].length == 2 && nueva[2].length == 4){
	           	valor = nueva[2]+'-'+nueva[1]+'-'+nueva[0];
            }
			var parametros = {
				"var_tabla": tabla,
				"var_campo" : campo,
				"var_valor" : valor,
				"var_id" : campo_id,
				"var_id_val" : valor_id
			};
			var url="../comunes/funcion_actualizarcampo.php"; 
			$.ajax
			({
				type: "POST",
			    url: url,
			    data: parametros,
			    success: function(data)
			    {
		            $("#resultado").html(data);
		            setTimeout(function() {
	        			$("#msg_act").fadeOut(1500);
	    			},3000);
			    	buscar();
			    }
			});
			return false; 
	}
}
function actualizar_dato(tabla,campo,valor,campo_id,valor_id)
{
    //para verificar si es una fecha voltearla al momento de guardarla
      var nueva;
      nueva = valor.split("-");
      if (valor.length == 10 && nueva[0].length == 2 && nueva[1].length == 2 && nueva[2].length == 4){
        valor = nueva[2]+'-'+nueva[1]+'-'+nueva[0];
      }
      var parametros = {
        "var_tabla": tabla,
        "var_campo" : campo,
        "var_valor" : valor,
        "var_id" : campo_id,
        "var_id_val" : valor_id
      };
      var url="../comunes/funcion_actualizarcampo.php"; 
      $.ajax
      ({
        type: "POST",
          url: url,
          data: parametros,
          success: function(data)
          {
           	
          }
      });
      return false; 
}
</script>

<?php
include_once('conexion.php');
if ($_POST!=NULL)
{
  	$tabla=$_POST['var_tabla'];
  	$nregistros = $_POST['var_nregistros'];
  	$pag_actual = $_POST['var_pag_actual'];
	$titulos = explode(',', $_POST[var_titulos_busqueda]);
	$datos = explode(',', $_POST[var_datos_busqueda]);
	$ordenar = $_POST[var_orden_busqueda];
	$edita_orden_num = $_POST[var_edita_orden_num];
  	foreach ($_POST as $nombre_campo=>$valor_campo) 
	{
		if (explode('_', $nombre_campo)[0]!='var'){
			if ($valor_campo && $nombre_campo!='pass_user' && $nombre_campo!='pass_user2' && $nombre_campo!='fech_regi' && $nombre_campo!='fech_actu'){
				//para verificar si es una fecha voltearla al momento de mostrarla
				$nueva = explode('-', $valor_campo);
	            if (strlen($valor_campo) == 10 && strlen($nueva[0]) == 2 && strlen($nueva[1]) == 2 && strlen($nueva[2]) == 4){
	            	$valor_campo = $nueva[2].'-'.$nueva[1].'-'.$nueva[0];
	            }
				$condicion_auto .= " AND ".$nombre_campo." LIKE '%".$valor_campo."%'";
			}
		}
		else{
			if ($nombre_campo=='var_where'){
				$var_where = explode(':::', $valor_campo);
				$var_where = $var_where[0].'='.$var_where[1];
			}
		}
	}
}
else {
	$titulos = explode(',', $titulos_busqueda);
	$datos = explode(',', $datos_busqueda);
	$ordenar = $orden_busqueda;
}

if ($_POST[var_where]){ $var_where = $_POST[var_where]; }
if ($var_where) { 
	$condicion_auto .= ' AND '.$var_where; 
}

if ($condicion_auto){
	$condicion .= "WHERE".$condicion_auto;
	$condicion = str_replace('WHERE AND', 'WHERE', $condicion);
}


//// para determinar numero de registros y paginado de la busqueda
if (!$nregistros) { $nregistros = 10; }
if (!$pag_actual) { $pag_actual = 1; }
///// para determinar desde que registro se mostrará
$desde_reg = (($pag_actual * $nregistros)-$nregistros);

/// Buscar el primary key
$sql_key = mysql_query( "SHOW FIELDS FROM $tabla " ) or trigger_error( mysql_error(), E_USER_ERROR );
while( $key = mysql_fetch_row( $sql_key ) ){
	if( $key[3]=='PRI' ) $primary_key = $key[0];  
} 
/// para determinar la cantidad total de registros
$buscar_pre="SELECT * from ".$tabla." ".$condicion."";
$busqueda_pre=mysql_query($buscar_pre);
$cantidad_total = mysql_num_rows($busqueda_pre);

/// si la cantidad de resultados es igual o mayor a la posición desde la que se desea mostrar se inicia desde 0 y se indica que es la primera pagina
if ($desde_reg >= mysql_num_rows($busqueda_pre)){ 
	$desde_reg = 0; 
	$pag_actual = 1;
}


/// para mostrar los resultados de la busqueda
$buscar="SELECT * from ".$tabla." ".$condicion." ORDER BY ".$primary_key." DESC LIMIT ".$desde_reg.", ".$nregistros;
/// Determinar si hay un orden especial
if ($ordenar){
	$buscar="SELECT * from ".$tabla." ".$condicion." ORDER BY ".$ordenar." LIMIT ".$desde_reg.", ".$nregistros;	
}
$busqueda=mysql_query($buscar);
if (mysql_num_rows($busqueda)>0){
	echo '<div class="container">';
	echo '<div class="titulo_form">Registros Encontrados</div>';
	echo '<div class="table-responsive">';
	echo '<table class="table table-striped table-bordered table-hover table-condensed">';
	echo '<tr class="info">';
	echo '<th width="40px;" class="text-right">&nbsp;#&nbsp;</th>';
	for ($i=0;$i<count($titulos);$i++){
		echo '<th>'.$titulos[$i].'</th>';
	}
	if($edita_orden_num){
		$colum_accion = 'colspan="3"';
	}
	echo '<th width="10em" '.$colum_accion.' class="text-center">Acciones</th>';
	echo '</tr>';
}
$contador = $desde_reg + 1;
while ($fila=mysql_fetch_array($busqueda)) 
{
	$param_eliminar = "'".$tabla."', '".$primary_key."', '".$fila[$primary_key]."'";
	$param_llenar = "'".$tabla."', '".$primary_key."', '".$fila[$primary_key]."'";
	$param_ocultar = "$('#confirmar".$contador."').removeClass('fade');"; 
	echo '	<div class="modal fade" id="confirmar'.$contador.'" tabindex="-1" rol="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				    <button title="Cerrar Ventana" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    <h4>Esta acción requiere su confirmación</h4>
				</div>
				<div class="modal-body">
					<span class="glyphicon glyphicon-question-sign glyphicon_azul pull-left msg_modal_icon"></span>
				    <p>¿Desea Eliminar el registro #'.$contador.'?</p>
				</div>
				<div class="modal-footer">
				    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
				    <button class="btn btn-primary" data-dismiss="modal" onclick="'.$param_ocultar.' eliminar_registro('.$param_eliminar.');">Aceptar</button>
				</div>
			</div>	
		</div>		
	</div>';
	echo '<tr>';
	echo '<th class="text-right">'.$contador.'&nbsp;</th>';
	for ($j=0;$j<count($datos);$j++){
			$campo_aux = explode('->', $datos[$j]);
			$cuenta_campo_aux = count($campo_aux);
			$nuevo_valor = $fila[$campo_aux[0]];
			if ($cuenta_campo_aux>1){
				for ($h=1;$h<$cuenta_campo_aux;$h++){
					$var = explode('::', $campo_aux[$h]);
					$sql = "SELECT * FROM ".$var[0]." WHERE ".$var[1]."='".$nuevo_valor."'";
					$bus = mysql_query($sql);
					$res = mysql_fetch_array($bus);
					$nuevo_valor = $res[$var[2]];	
				}
			}
			if ($nuevo_valor!='') { $fila[$datos[$j]] = $nuevo_valor; } else { $fila[$datos[$j]] = $fila[$campo_aux[0]]; }
			if (substr($fila[$datos[$j]], 0, 1) == '#' AND strlen($fila[$datos[$j]]) == 7 ){
				$add_color = '<div style="float: left; height: 20px; width: 20px; background-color:'.$fila[$datos[$j]].'; background-image: url(&#39;../imagenes/uploads/colores/'.$fila[imag_color].'&#39;);">&nbsp;</div>';
			}
		echo '<td>&nbsp;'.$fila[$datos[$j]].' '.$add_color.'</td>';
		$add_color = '';
	}
	if($edita_orden_num){
		echo '<td class="text-center" width="1.6em">';
			if($contador!=1){
				echo'<span class="boton_accion glyphicon glyphicon glyphicon-chevron-up" title="Subir" style="width: 1.5em; cursor:pointer;"  onclick="ordenar_orden(\''.$tabla.'\',\''.$edita_orden_num.'\','.$fila[$edita_orden_num].',\''.$primary_key.'\',\''.($fila[$primary_key]).'\',\'sube\');"> </span>';
			}
		echo '</td>';
		echo '<td class="text-center" width="1.6em">';
			if($contador!=$cantidad_total){
				echo'<span class="boton_accion glyphicon glyphicon glyphicon-chevron-down" title="bajar" style="width: 1.5em; cursor:pointer;" onclick="ordenar_orden(\''.$tabla.'\',\''.$edita_orden_num.'\','.$fila[$edita_orden_num].',\''.$primary_key.'\',\''.($fila[$primary_key]).'\',\'baja\');"> </span>';
			}
		echo '</td>';
	}
	echo '<td class="text-center" width="80px">&nbsp;'; 
	echo'<span class="boton_accion glyphicon glyphicon-edit" title="Editar Registro" style="width: 1.5em; cursor:pointer;" onclick="llenar_form('.$param_llenar.')"> </span> &nbsp;<span class="boton_accion glyphicon glyphicon-remove" style="cursor:pointer;"  title="Eliminar Registro" style="width: 1.5em;"  data-toggle="modal" data-target="#confirmar'.$contador.'"> </span>&nbsp;</td>';
	echo '</tr>';
	$contador++;
}
if (mysql_num_rows($busqueda)>0){
	echo '</table>
	</div>';

$npaginas = ceil(mysql_num_rows($busqueda_pre)/$nregistros);
if ($npaginas<1){ $npaginas = 1; }
$back = "$('#var_pag_actual').val('".($pag_actual-1)."');buscar();";
$next = "$('#var_pag_actual').val('".($pag_actual+1)."');buscar();";
if ($pag_actual==1){ $back = ''; $add_style_back = 'class="disabled"'; }
if ($pag_actual==$npaginas){ $next = ''; $add_style_next = 'class="disabled"'; }
?>

	<div class="row">
		<div class="col-xs-2 col-sm-2 col-md-2">
			<span class="etq_form">Mostrar:</span>
			<select class="form-control" id="sel1" onchange="$('#var_nregistros').val($(this).val());buscar()">
				<option <?php if ($nregistros==5) { echo 'selected'; } ?> value="5">5 Registros</option>
			    <option <?php if ($nregistros==10) { echo 'selected'; } ?> value="10">10 Registros</option>
			    <option <?php if ($nregistros==50) { echo 'selected'; } ?> value="50">50 Registros</option>
			    <option <?php if ($nregistros==100) { echo 'selected'; } ?> value="100">100 Registros</option>
			</select>
		</div>
		<div class="col">
			<nav>
			  <ul class="pagination">
			    <li onclick="<?php echo $back; ?>" <?php echo $add_style_back; ?>>
			      <a href="#<?php echo ($pag_actual-1); ?>" aria-label="Anterior">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>
			    <?php 
			    	$desde = 0;
			    	for ($k=1;$k<=$npaginas;$k++){ 
			    		if ($pag_actual==$k) { 
			    			$add_active='class="active"'; 
			    			$var_onclikpag = '';
			    		} else { 
			    			$add_active=''; 
			    			$var_onclikpag = "$('#var_pag_actual').val('".$k."');buscar();";
			    		}
			    		echo '<li '.$add_active.' onclick="'.$var_onclikpag.'"><a href="#'.$desde.'">'.$k.'</a></li>';
			    		$desde+= $nregistros; 
			    	}
			    ?>
			    <li onclick="<?php echo $next; ?>" <?php echo $add_style_next; ?>>
			      <a href="#<?php echo ($pag_actual+1); ?>" aria-label="Siguiente">
			        <span aria-hidden="true">&raquo;</span>
			      </a>
			    </li>
			  </ul>
			</nav>
		</div>
	</div>
</div>
<?php }
else { ?>
<script type="text/javascript">
	var mensaje = "</br><div class='alert alert-danger' id='msg_act'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='glyphicon glyphicon-alert pull-left'></span>&nbsp;&nbsp;<strong>No se han encontrados resultados para la busqueda especificada. Intente nuevamente con otros datos...</strong></div>";
   	$("#resultado").html(mensaje);
   	setTimeout(function() {
	   	$("#msg_act").fadeOut(1500);
	},4000);
</script>
<?php } ?>