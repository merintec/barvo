-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 12-07-2016 a las 21:59:30
-- Versión del servidor: 5.5.47-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `barvo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
  `id_cont` int(11) NOT NULL AUTO_INCREMENT,
  `nomb_ape_cont` varchar(250) NOT NULL,
  `email_cont` varchar(250) NOT NULL,
  `come_cont` text NOT NULL,
  `fech_cont` date NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`id_cont`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`id_cont`, `nomb_ape_cont`, `email_cont`, `come_cont`, `fech_cont`, `status`) VALUES
(1, 'Juan Marque', 'juanjmt@gmail.com', 'Hol como estan', '2016-07-10', 'pendiente'),
(2, 'Andres Marquez2', 'juanandresmr@gmail.com', 'Hol como estan2', '2016-07-10', 'procesado'),
(3, 'Andres Marquez3', 'juanandresmr@gmail.com', 'Hol como estan3', '2016-07-10', 'procesado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id_noti` int(11) NOT NULL AUTO_INCREMENT,
  `nomb_noti` varchar(250) NOT NULL,
  `desc_noti` varchar(250) NOT NULL,
  `imag_noti` text NOT NULL,
  `cont_noti` text NOT NULL,
  `stat_noti` char(1) NOT NULL,
  `defecto` varchar(1) NOT NULL,
  PRIMARY KEY (`id_noti`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id_noti`, `nomb_noti`, `desc_noti`, `imag_noti`, `cont_noti`, `stat_noti`, `defecto`) VALUES
(3, 'Inaguración de la Feria del Pomodoro', 'Inaguración de la Feria del Pomodoro', 'noti-002.jpg', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.', 'A', ''),
(4, 'Nuevos Sabores de Pizza', 'Pizza', 'noti-001.jpg', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. ', 'A', ''),
(5, 'Inaguración de la Feria del Pomodoro', 'Inaguración de la Feria del Pomodoro', 'noti-002.jpg', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.', 'A', ''),
(6, 'Inaguración de la Feria del Pomodoro', 'Inaguración de la Feria del Pomodoro', 'noti-002.jpg', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.', 'A', ''),
(8, 'Nuevas Noticiasdos', 'Noticia de Prueba por defecto', '', 'dasdasdasd ', 'D', 's');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id_prod` int(11) NOT NULL AUTO_INCREMENT,
  `nomb_prod` varchar(250) NOT NULL,
  `desc_prod` varchar(250) NOT NULL,
  `imag_prod` text NOT NULL,
  `cont_prod` text NOT NULL,
  `icon_prod` text NOT NULL,
  `defecto` varchar(1) NOT NULL,
  PRIMARY KEY (`id_prod`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_prod`, `nomb_prod`, `desc_prod`, `imag_prod`, `cont_prod`, `icon_prod`, `defecto`) VALUES
(1, 'Salsa Barvo', 'Salsa para Pasta Barvo', 'productos-salsa.png ', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'icono-tomate.png', 's'),
(2, 'Raviolis Barvo', 'Raviolis Rellenos Barvo', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recetas`
--

CREATE TABLE IF NOT EXISTS `recetas` (
  `id_rece` int(11) NOT NULL AUTO_INCREMENT,
  `nomb_rece` varchar(250) NOT NULL,
  `desc_rece` varchar(250) NOT NULL,
  `imag_rece` text NOT NULL,
  `cont_rece` text NOT NULL,
  `id_trece` int(11) NOT NULL,
  `stat_rece` char(1) NOT NULL,
  `icon_rece` text NOT NULL,
  `defecto` varchar(1) NOT NULL,
  PRIMARY KEY (`id_rece`),
  KEY `id_trece` (`id_trece`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `recetas`
--

INSERT INTO `recetas` (`id_rece`, `nomb_rece`, `desc_rece`, `imag_rece`, `cont_rece`, `id_trece`, `stat_rece`, `icon_rece`, `defecto`) VALUES
(16, 'Pasta Putanesca', 'Pasta Putanesca', 'receta-001.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo &nbsp; &nbsp; &nbsp;consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ', 2, 'A', 'icono-oregano.png', 's'),
(17, 'Pizza Margarita', 'Pizza Margariat', 'productos-salsa.png', '<p align="left">La <b>pizza</b> es una de esas comidas que nadie rechaza nunca. Aun estando a dieta, buscamos opciones saludables que se le parezcan. No podemos escapar de ella, la amamos.</p><p align="left">A lo largo de nuestras vidas, al menos una vez intentaremos aprender como hacer pizza, y seguramente prepararemos una en casa con amigos o familia. Y quede buena o no, la disfrutaremos y seremos felices.</p><p align="left">Posiblemente tengas noción de como hacer pizza. A lo mejor has visto a alguien prepararla o quizás no tienes ni idea pero quieres aprender.</p> ', 1, 'A', 'icon-cuchara.png', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subproductos`
--

CREATE TABLE IF NOT EXISTS `subproductos` (
  `id_subp` int(11) NOT NULL AUTO_INCREMENT,
  `nomb_sprod` varchar(250) NOT NULL,
  `desc_sprod` varchar(250) NOT NULL,
  `cont_sprod` text NOT NULL,
  `imag_sprod` text NOT NULL,
  `id_prod` int(11) NOT NULL,
  `icon_sprod` text NOT NULL,
  `defecto` varchar(1) NOT NULL,
  PRIMARY KEY (`id_subp`),
  KEY `id_prod` (`id_prod`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `subproductos`
--

INSERT INTO `subproductos` (`id_subp`, `nomb_sprod`, `desc_sprod`, `cont_sprod`, `imag_sprod`, `id_prod`, `icon_sprod`, `defecto`) VALUES
(3, 'Ravioli con Carne', 'Ravioli con Carne', '', '', 2, '', ''),
(4, 'Ravioli 4 Quesos', 'Ravioli 4 Quesos', '', '', 2, '', ''),
(5, 'Ravioli Capresa', 'Ravioli Capresa', '', '', 2, '', ''),
(6, 'Ravioli con Espinaca', 'Ravioli con Espinaca', '', '', 2, '', ''),
(7, 'Ravioli con Ajo Porro', 'Ravioli con Ajo Porro', '', '', 2, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_recetas`
--

CREATE TABLE IF NOT EXISTS `tipo_recetas` (
  `id_trece` int(11) NOT NULL AUTO_INCREMENT,
  `nomb_trece` varchar(250) NOT NULL,
  `desc_trece` varchar(250) NOT NULL,
  PRIMARY KEY (`id_trece`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `tipo_recetas`
--

INSERT INTO `tipo_recetas` (`id_trece`, `nomb_trece`, `desc_trece`) VALUES
(1, 'Pizzas', ''),
(2, 'Pastas', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del usuario',
  `tipoi_user` char(1) NOT NULL COMMENT 'tipo de identificacion',
  `numi_user` varchar(11) NOT NULL COMMENT 'numero de identificacion',
  `nom_ape_user` varchar(60) NOT NULL COMMENT 'Nombre y Apellido',
  `tele_user` varchar(11) DEFAULT NULL COMMENT 'Número de teléfono',
  `corre_user` varchar(100) NOT NULL COMMENT 'correo del usuario',
  `pass_user` text NOT NULL COMMENT 'contraseña del usuario',
  `pass_user2` text NOT NULL,
  `tipo_user` varchar(30) NOT NULL COMMENT 'tipo de usuario',
  `stat_user` varchar(30) NOT NULL COMMENT 'status del usuario activo o inactivo',
  `sex_user` varchar(30) DEFAULT NULL COMMENT 'sexo del usuario',
  `noti_user` int(1) DEFAULT NULL COMMENT '1 si desea recibir noticia',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `numi_user` (`numi_user`),
  UNIQUE KEY `corre_user` (`corre_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Esta tabla manejara todos los usuarios del sistema' AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_user`, `tipoi_user`, `numi_user`, `nom_ape_user`, `tele_user`, `corre_user`, `pass_user`, `pass_user2`, `tipo_user`, `stat_user`, `sex_user`, `noti_user`) VALUES
(1, 'V', '16657064', 'Juan Marquez', '04265772755', 'juanjmt@gmail.com', '88eb1c0803e43de250f54e4f2b99781d', '88eb1c0803e43de250f54e4f2b99781d', '1', 'activo', 'm', NULL),
(2, 'V', '17523582', 'Johanna Rondon', '04265772755', 'cupy22@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '827ccb0eea8a706c4c34a16891f84e7b', '1', 'activo', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id_video` int(11) NOT NULL AUTO_INCREMENT,
  `nomb_video` varchar(250) NOT NULL,
  `desc_video` varchar(250) NOT NULL,
  `cont_video` text NOT NULL,
  `stat_video` char(1) NOT NULL,
  PRIMARY KEY (`id_video`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id_video`, `nomb_video`, `desc_video`, `cont_video`, `stat_video`) VALUES
(1, 'Video Inicial', 'Video Inicial', 'n4MrDXLUrfg', 'A'),
(2, 'Video Inicial2', 'Video Inicial2', 'n4MrDXLUrfg', 'A');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `recetas`
--
ALTER TABLE `recetas`
  ADD CONSTRAINT `recetas_ibfk_1` FOREIGN KEY (`id_trece`) REFERENCES `tipo_recetas` (`id_trece`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `subproductos`
--
ALTER TABLE `subproductos`
  ADD CONSTRAINT `subproductos_ibfk_1` FOREIGN KEY (`id_prod`) REFERENCES `productos` (`id_prod`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
