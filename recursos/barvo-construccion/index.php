﻿<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title>.::: Productos Barvo c.a. :::.</title>
 
<link href="comunes/estilos.css" rel="stylesheet" type="text/css">

<!-- html5.js - IE  9 -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
 
<!-- css3-mediaqueries.js - IE  9 -->
<!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
 
</head>
<body>
<div id="pagewrap">
  	<div id="titulo">Nuestra página se encuentra en construcción.<br>Vuelve Pronto y permítenos sorprenderte</div>
	<div id="logo"><img src="imagenes/logo.png" title="Productos Barvo c.a." width="100%"></div>
	<div id="tendereta">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
			  <td width="30%"><img src="imagenes/icono-tomate.png" title="Productos Barvo c.a." width="80%"></td>
			  <td width="20%">&nbsp;</td>
			  <td width="30%"><img src="imagenes/icono-ravioli.png" title="Productos Barvo c.a." width="80%"></td>
			</tr>
		</table>
	</div>
	<div id="bebes">Salsas, raviolis y mucho mas...</div>
</div>
<div id="social_net">
	<a href="https://www.facebook.com/Barvo-349570851740530/?ref=br_rs" target="NEW"><div id="btn" class="face">&nbsp;</div></a>
	<a href="https://instagram.com/barvoca/" target="NEW"><div id="btn" class="insta">&nbsp;</div></a>
	<a href="mailto:productosbarvo@gmail.com"><div id="btn" class="mail">&nbsp;</div></a>
	<a><div id="btn" class="tele" title="0274-7899110">&nbsp;</div></a>
	<div id="telefono">Teléfono: 0274-7899110<br>e-mail: productosbarvo@gmail.com</div>
</div>
</body>
</html>
