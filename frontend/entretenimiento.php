<?php 
    include_once("comunes/conexion.php");
    $indicer = 0;
    /*
    $sql_tipos = "SELECT * FROM tipo_recetas tr, recetas rc WHERE tr.id_trece=rc.id_trece AND rc.stat_rece='A' GROUP BY tr.orden ORDER BY nomb_trece ";
    $bus_tipos = mysql_query($sql_tipos);
    while($reg_tipos = mysql_fetch_array($bus_tipos)){*/
    $sql_rece = "SELECT * FROM recetas WHERE stat_rece='A' ORDER BY defecto desc,orden";
      $bus_rece = mysql_query($sql_rece);
      while($reg_rece = mysql_fetch_array($bus_rece)){
        $recetas[$indicer][id]=$reg_rece[id_rece];
        $recetas[$indicer][imagen]='/recetas/'.$reg_rece[imag_rece];
        $recetas[$indicer][nombre]=$reg_rece[nomb_rece];
        $recetas[$indicer][contenido]=substr($reg_rece[cont_rece], 0, 700).'...';
        $indicer++;
      }
    /*}*/

    //consulta de videos

    $indicev = 0;
    $sql_videos= "SELECT * FROM videos WHERE  stat_video='A'  ORDER BY orden";
    $bus_videos = mysql_query($sql_videos);
    while($reg_videos = mysql_fetch_array($bus_videos))
    {
        $video[$indicev][contenido]=$reg_videos[cont_video];
        $video[$indicev][titulo]=$reg_videos[desc_video];
        $video[$indicev][texto]=$reg_videos[text_video];
        $indicev++;

    }


?>
<html lang="es">
  <body>
  	<div class="entretenimiento">
      <div class="container-fluid">
      	<div class="row">
            <div class="col-md-4 col-xs-6">
              <img id="entretenimiento" class="globo_productos" src="imagenes/site/globo-entretenimiento.png" >
            </div>
            <div class="col-md-6 hidden-xs"> <?php include("menu_entretenimiento.php"); ?> </div>
            <div class="col-xs-3 visible-xs"></div>

            <div class="col-md-1 col-xs-1">
            </div>
            <div class="col-md-1 col-xs-1">  

            </div>
            <div id="entre_cel" class="visible-xs"><a onclick="$('#videos').hide(); $('#recetas').show();" href="#entretenimiento">Recetas</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="$('#videos').show(); $('#recetas').hide();" href="#entretenimiento">Videos</a></div>
          </div>
 <!-- Swiper -->
    <div id="recetas" class="swipper-prod">
      <div id="swiper3" class="swiper-container">
          <div class="swiper-wrapper">
              <?php for($i=0;$i<$indicer;$i++){ ?>
                  <div class="swiper-slide">
                    <div class="swipper-scroll">
                      <div class="row">
                        <div class="col-md-1 col-xs-1" ></div>
                        <div class="col-md-5 col-sm-5 col-xs-10" >
                           <img class="imagen_prod" src="imagenes/uploads<?php echo $recetas[$i][imagen];  ?>" > 
                        </div>
                        <div class="visible-xs col-xs-12"></div>
                        <div class="visible-xs col-xs-1"></div>
                        <div class="col-md-5 col-xs-10 espacio_parrafo_prod">
                          <img src="imagenes/site/icon-cuchara.png" class="cuchara">
                          <center><span class="titulo_parrafo"><?php echo strtoupper($recetas[$i][nombre]);  ?>  </span><center>
                          <hr class="linea-gris">
                          <p class="parrafo">  <?php echo $recetas[$i][contenido];  ?></p>
                          <div class="col-md-12 col-xs-12" align="center">
                            <button class="fondo_leer" data-toggle="modal" data-target="#notibarvo" onclick="abrir_receta(<?php echo $recetas[$i][id]; ?>)" >Leer más</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              <?php } ?>
          </div>
          <!-- Add Pagination -->
          <!-- <div class="swiper-pagination"></div> -->
          <!-- Add Arrows -->
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
      </div>
    </div>



<!-- Swiper -->
    <div id="videos" class="swipper-prod">
      <div id="swiper4" class="swiper-container">
          <div class="swiper-wrapper">
              <?php for($i=0;$i<$indicev;$i++){ ?>
                  <div class="swiper-slide">
                    <div class="contenedor-videos">
                      <div class="row">
                        <div class="col-md-7 col-xs-12">
                          <div class="video-responsive">
                            <?php 
                                echo ' <iframe width="500px" height="315px" src="https://www.youtube.com/embed/'.$video[$i][contenido].'?rel=0&controls=1&showinfo=0" frameborder="0" allowfullscreen></iframe>';
                            ?>
                          </div>
                        </div>
                        <div class="col-md-5 col-xs-12">
                          <div class="titulo_parrafo"><?php echo $video[$i][titulo]; ?></div>
                          <div class="parrafo "><?php echo $video[$i][texto]; ?></div>
                        </div> 
                      </div>

                    </div>
                  </div>
              <?php } ?>
          </div>
          <!-- Add Pagination -->
          <!-- <div class="swiper-pagination"></div> -->
          <!-- Add Arrows -->
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
      </div>
    </div>



      </div>
    </div>


 </body>
</html>