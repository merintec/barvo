<html>
<body>
<div class="somos">
      <div class="container-fluid">

  			<div class="row">
  				<div class="col-md-4 col-xs-6"> 
  					<img id="somos" class="globo_somos" src="imagenes/site/globo-somos.png" >
  				</div>
  				<div class="col-md-6 col-xs-3"> </div>
  			   <div class="col-md-1 col-xs-1">
  				</div>
  				<div class="col-md-1 col-xs-1"> 
  				</div>

  			</div>
  			<div class="row">
  				<div class="col-md-6 col-xs-4">
  					<img class="imgquienes" src="imagenes/site/img-quienes.png" >
  				</div>
         
  				<div class="col-md-5 col-xs-8 espacio_parrafo">
              <span class="titulo_parrafo"> ¿QUIENES SOMOS? </span>
              
              <p align="justify" class="parrafo">

              Somos una fábrica de alimentos especializada en salsas para pastas y pastas frescas congeladas, dirigidas al segmento que busca variedad de opciones de alimentación rápidas y sencillas que les den el tiempo libre para disfrutar de las actividades que aman. Para ello contamos con un equipo de trabajo innovador y dedicado a ofrecer productos de excelente calidad.

              </p>
              <br class="hidden-xs">
              <br class="hidden-xs">
              <span class="titulo_parrafo"> ¿POR QUÉ CONFIAR EN BARVO? </span>
              <p align="justify" class="parrafo"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>  
              <br>
             <br class="hidden-xs">
             <center> <img class="imgparrafo" src="imagenes/site/icono-oregano.png" > </center>

          
  				</div>
         
  			</div>

      </div>

</div>
</body>
</html>
