<?php 
    include_once("comunes/conexion.php");
?>
<html lang="es">
<script src="validacion/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>
<script src="validacion/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="validacion/css/validationEngine.jquery.css" type="text/css"/>
<link rel="stylesheet" href="validacion/css/template.css" type="text/css"/>
<script type="text/javascript">
    function enviar(){
        if ($("#contactform").validationEngine('validate')){
            var url="js/phpmailer/envio.php"; 
            var parametros = {
                "nombre"   : $('#nombre').val(),
                "correo"   : $('#correo').val(),
                "contenido": $('#contenido').val(),
                "telefono": $('#telefono').val()
            };
            $.ajax
            ({
                type: "POST",
                url: url,
                data: parametros,
                beforeSend: function () {
                    $("#enviar_loading").show();
                    var espera = "Enviando Mensaje. Por favor espere...<br><br>";
                    $("#enviar_msg").html(espera);
                },

                success: function(datatemp)
                {
                    console.log(datatemp);
                    $("#enviar_loading").hide();
                    datatemp=datatemp.split(":::");
                    var codigo=datatemp[0];
                    var mensaje=datatemp[1];
                    $("#enviar_msg").html(mensaje);
                    setTimeout(function() {
                      $("#mensaje").fadeOut(1500);
                    },3000);
                    if (codigo==001){
                        $("#contactform")[0].reset();
                    }
                }
            });
            return false;
        }
    }
</script>
  <body>
  	<div class="contacto">
      <div class="container-fluid">
      	<div class="row">
            <div class="col-md-4 col-xs-6">
              <img id="contacto" class="globo_productos" src="imagenes/site/globo-contacto.png">
            </div>
            <div class="col-md-6 hidden-xs"></div>
            <div class="col-xs-3 visible-xs"></div>

            <div class="col-md-1 col-xs-1">
            </div>
            <div class="col-md-1 col-xs-1">
            </div>
        </div>

        <div class="row" >

            <div class="col-md-6 hidden-xs">
            </div>
            <div class="col-md-6 col-xs-12">
              <div class="row posicion_form">
                 <div class="col-md-10 col-xs-12">
                    <span class="parrafo_contacto">  Nuestro equipo agradece tu visita. Ponte en contacto con nosotros y con gusto te ayudaremos a resolver tus dudas o comentarios. </span>
                 </div>
              </div>
              <form id="contactform"  method="POST" name="contactform" onsubmit="return jQuery(this).validationEngine('validate');">
                <div class="row">
                   <div class="col-md-10 col-xs-12">
                      <div class="form-group">
                        <br>
                        <input type="text" name='nombre' id='nombre' class="validate[required, custom[onlyLetterSp], minSize[3], maxSize[50]] text-input, form-control, campo_form" placeholder='NOMBRE Y APELLIDO'>
                      </div>
                   </div>
                </div>
                 <div class="row">
                   <div class="col-md-10 col-xs-12">
                      <div class="form-group">
                        <input type="email" name='correo' id='correo' class="validate[required, custom[email], minSize[3], maxSize[100]] text-input, form-control, campo_form" placeholder='EMAIL'>
                      </div>
                   </div>
                </div>
                 <div class="row">
                   <div class="col-md-10 col-xs-12">
                      <div class="form-group">
                        <input type="text" name='telefono' id='telefono' class="validate[required, custom[phone], minSize[3], maxSize[100]] text-input, form-control, campo_form" placeholder='TELÉFONO'>
                      </div>
                   </div>
                </div>
                <div class="row">
                   <div class="col-md-10 col-xs-12">
                      <div class="form-group">
                        <div class="text_form_contenedor">
                          <textarea name="contenido" id="contenido" rows="5" class=" validate[required], text-input  form-control, text_form"  placeholder="COMENTARIO"></textarea>
                        </div>
                      </div>
                   </div>
                </div>
                <div class="col-md-10 col-xs-12" id="enviar_msg"></div>
                 <div class="row">
                   <div class="col-md-10 col-xs-12">
                      <center> <button class="fondo_enviar" onclick="enviar();return false;"><span id="enviar_loading"><img src="imagenes/site/loading.gif">&nbsp;</span>Enviar&nbsp;<span class="glyphicon glyphicon-send"></span></button> </center>
                      <script type="text/javascript">$('#enviar_loading').hide();</script>
                   </div>
                </div>
              </form>
              <div class="row">
                 <div class="col-md-12 col-xs-9 pull-right">
                    <br>
                    <span class="parrafo_contacto">
                      Mérida - Venezuela. <br> 
                      info@barvo.com.ve - ventas@barvo.com.ve<br>
                      +58 274-789.91.10
                      <br><br><br><br>
                      <br class="visible-lg">
                      <br class="visible-lg">
                      <br class="visible-lg">
                    </span>
                 </div>
              </div>
            </div>
        </div>
        



      </div>
    </div>
    <div class="contacto-footer">
      <div class="footer-oregano">
        <img class="imgparrafo" src="imagenes/site/icono-oregano.png" >
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-9 col-xs-5">
            <div class="parrafo parrafo_footer">
              <p><b>BARVO</b>, posee la propiedad intelectual de todas las fotografías,<br> logotipos, y demás elementos expuestos en este sitio web &copy; <?php echo date(Y); ?>. <br>Todos los derechos reservados.</p><p>Políticas y privacidad.</p><p>J-40027708-6</p>
            </div>
          </div>
          <div class="col-md-3 col-xs-7">
            <div class="redes_sociales_footer pull-right">
              <a href="https://www.facebook.com/Barvo-349570851740530/?ref=br_rs" target="new"><img class="icon" src="imagenes/site/icon-face-white.png"></a>
              <a href="" target="new"><img class="icon" src="imagenes/site/icon-twitter-white.png"></a>
              <a href="https://instagram.com/barvoca/" target="new"><img class="icon" src="imagenes/site/icon-instagram-white.png"></a>
            </div>
          </div>
        </div>
        <div class="row" id="merintec">
            Diseñado por Lorelba Mazzola. Desarrollado por <a target="new" href="http://www.merintec.com.ve">Merintec</a>
        </div>
      </div>
    </div>
    <div class="contacto-logo-final">
      <div class="row footer-contenedor-logo">
        <img src="imagenes/site/logo.png"  class="logo" >
      </div>
    </div>
 </body>
</html>